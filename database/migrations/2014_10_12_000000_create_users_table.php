<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('confirm_code')->nullable();
            $table->string('name');
            $table->string('phone');
            $table->string('password');
            $table->string('email')->unique();
            $table->timestamp('request_confirm_code_date')->nullable();
            $table->double('latitude')->nullable();
            $table->double('longitude')->nullable();
            $table->timestamp('email_verified_at')->nullable();
            $table->string('request_new_email')->nullable();
            $table->text('device_id')->nullable();
            $table->string('lang')->default('ar');
            $table->string('role')->default('user')->comment('user|shop');
            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
