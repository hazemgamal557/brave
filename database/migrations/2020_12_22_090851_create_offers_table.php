<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOffersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('offers', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('product_name');
            $table->double('price_before');
            $table->double('price_after');
            
            $table->text('image');
            $table->unsignedBigInteger('shop_id')->index();
            $table->foreign('shop_id')
            ->references('id')
            ->on('shops')
            ->onDelete('cascade');          

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('offers');
    }
}
