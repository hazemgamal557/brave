<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Faker\Factory as Faker;
class CategorySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // $faker = Faker::create();
        // foreach(range(1, 20) as $index){
        // 	DB::table('categories')->insert([
        // 		'name' => $faker->word,
        // 		'image' => '/storage/images/head.png'
        // 	]);

        // }
        echo "🚦 start seeding CategorySeeder\n";
        $this->seedCategories();
    }
    private function seedCategories()
    {
        echo "🕛 categories";
        $records = [
            [
                'id'      => 1,
                'name' => 'clothes',
                'image'   => 'category/init/clothes.jpg'
            ],
            [
                'id'      => 2,
                'name' => 'resturants',
                'image'   => 'category/init/resturants.png'
            ],
            [
                'id'      => 3,
                'name' => 'electronic',
                'image'   => 'category/init/electronic.jpg'
            ],
            [
                'id'      => 4,
                'name' => 'book store',
                'image'   => 'category/init/book_store.jpg'
            ],
            [
                'id'      => 5,
                'name' => 'healthy foods',
                'image'   => 'category/init/healthy_foods.jpg'
            ],
        ];

        \App\Models\Category::insert($records);
        echo " 👍\n";
    }
}
