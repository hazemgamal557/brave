<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Faker\Factory as Faker;
class OfferSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // $faker = Faker::create();
        // foreach(range(1, 7) as $index)
        // {
        //     DB::table('offers')->insert([
        //         'product_name' => $faker->word,
        //         'price_before' => $faker->numberBetween($min = 1000, $max = 9000),
        //         'price_after' => $faker->numberBetween($min = 1000, $max = 9000),
        //         'image' => $faker->word,
        //         // 'expired_date' => $faker->dateTime($max = 'now', $timezone = null),
        //         'shop_id' => 2
        //     ]);
        // }
        echo "🚦 start seeding OfferSeeder\n";
        $this->seedOffers();

    }
    private function seedOffers()
    {
        $records = [
            [
                'id' => 1,
                'product_name' => 'shirt',
                'price_before' => 50,
                'price_after' => 30,
                'image' => 'offers/init/shirt.jpg',
                'shop_id' => 1,

            ],
            [
                'id' => 2,
                'product_name' => 'jacket',
                'price_before' => 70,
                'price_after' => 65,
                'image' => 'offers/init/jacket.jpg',
                'shop_id' => 1,

            ],
            [
                'id' => 3,
                'product_name' => 'shoese',
                'price_before' => 80,
                'price_after' => 75,
                'image' => 'offers/init/shoese.jpg',
                'shop_id' => 1,

            ],
            [
                'id' => 4,
                'product_name' => 'watch',
                'price_before' => 100,
                'price_after' => 70,
                'image' => 'offers/init/watch.jpg',
                'shop_id' => 1,

            ],
            [
                'id' => 5,
                'product_name' => 'cap',
                'price_before' => 20,
                'price_after' => 15,
                'image' => 'offers/init/cap.jpg',
                'shop_id' => 1,

            ],
            [
                'id' => 6,
                'product_name' => 'Burger',
                'price_before' => 20,
                'price_after' => 15,
                'image' => 'offers/init/burger.jpg',
                'shop_id' => 2,
            ],
            [
                'id' => 7,
                'product_name' => 'checken',
                'price_before' => 20,
                'price_after' => 15,
                'image' => 'offers/init/checken.jpg',
                'shop_id' => 2,
            ],
            [
                'id' => 8,
                'product_name' => 'checken',
                'price_before' => 20,
                'price_after' => 15,
                'image' => 'offers/init/checken.jpg',
                'shop_id' => 2,
            ],
            [
                'id' => 9,
                'product_name' => 'french Frize',
                'price_before' => 30,
                'price_after' => 25,
                'image' => 'offers/init/french_frize.jpg',
                'shop_id' => 2,
            ],
            [
                'id' => 10,
                'product_name' => 'macNgates',
                'price_before' => 30,
                'price_after' => 25,
                'image' => 'offers/init/macNgates.jpg',
                'shop_id' => 2,
            ],
            [
                'id' => 11,
                'product_name' => 'burger',
                'price_before' => 100,
                'price_after' => 75,
                'image' => 'offers/init/burgerKen.jpg',
                'shop_id' => 3,
            ],
            [
                'id' => 12,
                'product_name' => 'french fries',
                'price_before' => 100,
                'price_after' => 75,
                'image' => 'offers/init/french_fries.jpg',
                'shop_id' => 3,
            ],

            [
                'id' => 13,
                'product_name' => 'checken',
                'price_before' => 100,
                'price_after' => 75,
                'image' => 'offers/init/checkKen.jpg',
                'shop_id' => 3,
            ],
        ];
        \App\Models\Offer::insert($records);
        echo " 👍\n";
    }
}
