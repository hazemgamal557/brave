<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Faker\Factory as Faker;
class ShopSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // $faker = Faker::create();
        // foreach(range(1, 20) as $index){
        //     DB::table('shops')->insert([
        //        "name" => $faker->word,
        //        "address"  => $faker->paragraph,
        //     'bio' => $faker->randomDigit,
        //     'description' => $faker->paragraph,
        //     'code' => $faker->randomDigit,
        //        'coupon_limit' => $faker->randomDigit,
        //        'rating' => $faker->randomDigit,
        //        'user_id' => 1,
        //        'city_id' => 1

        //     ]);
        // }
        echo "🚦 start seeding shopSeeder\n";
        $this->seedShops();
    }
    public function seedShops()
    {
        echo "🕛 shops";
        $records = [
            [
            'id'   => 1,
            'name' => 'Adidas',
            'bio'  => 'sports clothes',
            'image'=> 'shops/init/adidas.png',
            'code' => 2020,
            'coupon_limit' => 3,
            'latitude'  => 5.22551,
            'longitude'  => 3.2525,
            'user_id'   => 2,
            'category_id'  =>  1,
            'rating'   => 5,
            'city_id' => 1
            ],
            [
                'id'   => 2,
                'name' => 'macdonald\'s',
                'bio'  => 'eats shop',
                'image'=> 'shops/init/macdonald.png',
                'code' => 2020,
                'coupon_limit' => 3,
                'latitude'  => 5.22551,
                'longitude'  => 3.2525,
                'user_id'   => 1,
                'category_id'  =>  2,
                'rating'   => 5,
                'city_id' => 3
            ],
            [
                'id'   => 3,
                'name' => 'KFC',
                'bio'  => 'eats shop',
                'image'=> 'shops/init/KFC.png',
                'code' => 2020,
                'coupon_limit' => 3,
                'latitude'  => 5.22551,
                'longitude'  => 3.2525,
                'user_id'   => 3,
                'category_id'  =>  2,
                'rating'   => 3,
                'city_id' => 6
            ],
        ];

        \App\Models\Shop::insert($records);
        echo " 👍\n";

    }
}
