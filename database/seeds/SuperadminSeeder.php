<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class SuperadminSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('admins')->insert([
            'email' => 'admin@admin.com',
            'name' => 'Superadmin',
            'password' => bcrypt('password'),
            'created_at'=> now()
        ]);
    }
}
