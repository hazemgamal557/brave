<?php

use Illuminate\Database\Seeder;
use App\Models\User;
use App\Models\Setting;
use App\Models\Shop;
use App\Models\Admin;
use App\Models\Category;
use App\Models\Coupon;
use App\Models\City;
use App\Models\Offer;
class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {

        // factory(Category::class, 30)->create();
        factory(Admin::class, 1)->create();
        factory(City::class, 30)->create();
        factory(User::class, 20)->create();
        // factory(Setting::class, 10)->create();
        $this->call([
            SettingSeeder::class,
            CategorySeeder::class,
            ShopSeeder::class,
            OfferSeeder::class
        ]);

        // factory(Category::class, 30)->create()->each(function ($user) {
        //     $user->shops()->saveMany(factory(Shop::class, 20)->make());
        // });

        // factory(Shop::class, 10)->create()->each(function ($user) {
        //     $user->offers()->saveMany(factory(offer::class, 20)->make());
        // });


        // factory(Coupon::class, 10)->create();



    }
}
