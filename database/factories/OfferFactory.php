<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Offer;
use App\Models\Shop;
use Faker\Generator as Faker;

$factory->define(Offer::class, function (Faker $faker) {
    return [
        'product_name' => $faker->word,
        'price_before' => $faker->randomDigit,
        'price_after' => $faker->randomDigit,
        'image' => '',
        
        'shop_id' => function()
        {
            return Shop::all()->random();
        }

    ];
});
