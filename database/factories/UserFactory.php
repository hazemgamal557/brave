<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\User;
use Faker\Generator as Faker;
use Illuminate\Support\Str;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(User::class, function (Faker $faker) {
    return [
        'confirm_code' => null,
        'name' => $faker->name,
        'phone' => $faker->phoneNumber,
        'email' => $faker->email,
        'password' => bcrypt('secret'),
        'request_confirm_code_date' => null,
        'request_new_email' => null,
        'latitude' => $faker->randomFloat($nbMaxDecimals = NULL, $min = 0, $max = NULL),
        'longitude' => $faker->randomFloat($nbMaxDecimals = NULL, $min = 0, $max = NULL),
        'email_verified_at' => null,
        'role' => 'user',
        'device_id' => 0
    ];
});
