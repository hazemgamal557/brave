<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Favourite;
use App\Models\User;
use App\Models\Offer;
use Faker\Generator as Faker;

$factory->define(Favourite::class, function (Faker $faker) {
    return [
        'user_id' =>  function()
        {
            return User::all()->random();
        },
        'offer_id' =>  function()
        {
            return Offer::all()->random();
        },
    
    ];
});
