<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Shop;
use App\Models\User;
use App\Models\City;
use App\Models\Category;
use Faker\Generator as Faker;

$factory->define(Shop::class, function (Faker $faker) {
    return [
        "name" => $faker->word,
            //    "address"  => $faker->paragraph,
            'bio' => $faker->randomDigit,
               'rating' => $faker->numberBetween(0, 5),
               'image' => '',
               'latitude' => $faker->randomFloat($nbMaxDecimals = NULL, $min = 0, $max = NULL),
               'longitude' => $faker->randomFloat($nbMaxDecimals = NULL, $min = 0, $max = NULL),
               'coupon_limit' => $faker->randomDigit,
               'code' => $faker->randomDigit,
               'verified' => 1,
            'user_id' => function()
            {
                return User::all()->random();
            },
            'city_id' => function()
            {
                return City::all()->random();
            },
            'category_id' => function()
            {
                return Category::all()->random();
            }
    ];
});
