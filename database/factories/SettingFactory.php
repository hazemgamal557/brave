<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Setting;
use Faker\Generator as Faker;

$factory->define(Setting::class, function (Faker $faker) {
    return [
        
        'gate' => 'gate',
        'value_ar' => 'value',
        'value_en' => 'value',
        
    ];
});
