<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Model;
use App\Models\Coupon;
use App\Models\User;
use App\Models\Shop;
use App\Models\Offer;
use Faker\Generator as Faker;

$factory->define(Coupon::class, function (Faker $faker) {
    return [
        'rating' => $faker->numberBetween(0, 5),
        'coupon' => $faker->numberBetween(1000, 9000),
        'user_id' => function()
        {
            return User::all()->random();
        },
        
        'shop_id' => function()
        {
            return Shop::all()->random();
        }
    ];
});
