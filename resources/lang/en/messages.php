<?php

return [
    // auth-controller
    'no-inactive-user-have-phone' => 'there is no inactive user have this phone number.',
    'no-active-user-have-phone' => 'there is no active user have this phone number.',
    'confirm-code-sent' => 'confirmation code has been sent successfully.',
    'confirm-code-expired' => 'confirm code does not exists or expired.',
    'confirm-code-not-valid' => 'confirm code is not valid.',
    'user-activated' => 'user have been activated successfully',
    'logged-in' => 'user has been logged in successfully.',
    'logged-out' => 'user has been logged out successfully.',
    'you-are-not-authenticated' => 'you are not authenticated',
    'category-has-been-added'=> 'category has been added',
    'category-has-been-updated' => 'category has been updated',
    'category-has-been-deleted' => 'category has been deleted',
    'city-has-been-added' => 'city has been added',
    'city-has-been-updated' => 'city has been updated',
    'city-has-been-deleted' => 'city has been deleted',
    'you-have-already-used-the-coupon' => 'you have already used the coupon',
    'coupon-has-been-used-maximum-times' => 'coupon has been used maximum times',
    'coupon-has-been-used-successfully' => 'coupon has been used successfully',
    'shop-has-been-rated' => 'shop has been rated',
    'item-is-not-valid' => 'item is not valid',
   'item-has-been-already-added' => 'item has been already added',
    'item-has-been-deleted' => 'item has been deleted',
    'your-shop-is-not-verified-yet' => 'your shop is not verified yet',
    'offer-has-been-added' => 'offer has been added',
    'offer-has-been-updated' => 'offer has been updated',
    'offer-has-been-deleted' => 'offer has been deleted',
    'sorry-shop-was-not-found' => 'sorry, shop was not found',
    'you-already-created-a-shop' => 'you have already created a shop',
    'shop-has-been-created' => 'shop has been created',
    'shop-is-not-verified' => 'shop is not verified',
    'shop-has-been-updated' => 'shop has been updated',
    'shop-has-been-deleted' => 'shop has been deleted',
    'shop-has-been-verified' => 'shop has been verified',
    'name-has-been-updated' => 'shop has been updated',
    'user-has-been-deleted' => 'user has been deleted',
    'item-has-been-added-to-favourites'=>'Item has been added to favourites' ,
    'coupon-is-not-right' => 'coupon is not right or does not exist' ,




    // data
    'male' => 'Male',
    'female' => 'Female',


    // general
    'updated-sucessfully' => 'Updated Successfully',
];