<?php

namespace App\Services;

use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Storage;

class FileManager
{
    public function uploadCategoryImage(UploadedFile $uploadedFile, String $prevFileName = '')
    {
        return $this->uploadFixedSizeImage($uploadedFile, $prevFileName, 'store', 'stores', 100, 100);

    }

    public function uploadOfferImage(UploadedFile $uploadedFile, String $prevFileName = '')
    {
        return $this->uploadFixedSizeImage($uploadedFile, $prevFileName, 'offer', 'offers', 100, 100);

    }
    

    public function uploadShopImage(UploadedFile $uploadedFile, String $prevFileName = '')
    {
        return $this->uploadFixedSizeImage($uploadedFile, $prevFileName, 'shop', 'shops', 100, 100);

    }
    private function uploadFixedSizeImage(UploadedFile $uploadedFile, string $prevFileName, string $prefix, string $directory, int $width = 300, int $height = 300)
    {
        // delete the previous file if exists.
        if (!empty($prevFileName) && is_file( storage_path("app/public/{$prevFileName}") )) {
           $this->remove($prevFileName);
        }

        // generate the new file name.
        $newFileName = 
            "{$prefix}-"
            . uniqid(time() . '-', true)
            . substr(pathinfo($uploadedFile->getClientOriginalName(), PATHINFO_FILENAME), 0, 10)
            . '.'
            . $uploadedFile->getClientOriginalExtension();

        // prepare the image file size
        $streamFileImage = \Image::make($uploadedFile)->fit($width, $height)->stream();

        // store the file
        Storage::put("public/{$directory}/{$newFileName}", $streamFileImage);
        return "{$directory}/$newFileName";
    }

} 