<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Coupon extends Model
{
    protected $fillable = [
        'user_id',
        'shop_id',
        'rate',
        'coupon',
        'created_at',
    ];
    protected $table = 'coupon_user';

}
