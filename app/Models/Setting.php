<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Setting extends Model
{
    protected $fillable  = ['gate', 'value_ar', 'value_en'];
}
