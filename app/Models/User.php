<?php

namespace App\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Sanctum\HasApiTokens;

class User extends Authenticatable
{

    use HasApiTokens, Notifiable;
    const CONFIRM_CODE_EXPIRES_IN_SEC = 60;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'email',
        'password',
        'role',
        'phone'

    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];


    public function coupon()
    {
    	return $this->belongsToMany(Offer::class,
    		'coupon_user',
    		'user_id',
    		'offer_id'
    	);
    }

    public function shop()
    {
        return $this->hasOne(Shop::class, 'user_id');
    }

    public function favourites()
    {
        return $this->hasMany(Favourite::class, 'user_id');
    }
    public function distance()
    {
        return $this->hasOne(Distance::class, 'user_id');
    }

    // public function scopeActivatedByCode($query)
    // {
    //     return $query->where('activated_by_code', true);
    // }

    public function isConfirmCodeActive()
    {
        return
            $this->request_confirm_code_date
                &&
            now()->diffInSeconds(
                now()->parse($this->request_confirm_code_date)
            ) <= self::CONFIRM_CODE_EXPIRES_IN_SEC;
    }
}
