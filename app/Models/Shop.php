<?php

namespace App\Models;


use Illuminate\Database\Eloquent\Model;

class Shop extends Model
{
    protected $fillable = [
        "name", 
    "bio", 
    "address", 
    "image", 
    "lat", 
    "long", 
    "user_id", 
    "category_id", 
    "rating", 
    "code", 
    "coupon_limit", 
    "city_id", 
    "verified"
];
    protected $hidden = ["pivot"];
    
    // public function category()
    // {
        // 	return $this->belongsToMany(Category::class,
        // 	 'cat_shop', 
    // 	 'shop_id', 
    // 	 'category_id'
    // 	);
    // }
    public function getImageAttribute()
    {
        if (empty($this->attributes['image'])) {
            return asset('storage/temp_shop/shop.png');
        }else{
            return asset("storage/{$this->attributes['image']}");
        }
    }
    public function category()
    {
        return $this->belongsTo(Category::class);
    }


    public function distance()
    {
        return $this->hasOne(Distance::class, 'shop_id');
    }
    

    public function offers(){
    	return $this->hasMany(Offer::class, 'shop_id');
    }


    public function user()
    {
        return $this->belongsTo(User::class);
    }


    public function city()
    {
        return $this->belongsTo(City::class);
    }

   

}
