<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    protected $fillable = ['name', 'image'];

    // protected $hidden = ["pivot"];
    // protected $table = "categories";
    public function getImageAttribute()
    {
        if (empty($this->attributes['image'])) {
            return asset('storage/category/interface.png');
        }else{
            return asset("storage/{$this->attributes['image']}");
        }
    }
    public function shops()
    {
        return $this->hasMany(Shop::class, 'category_id');
    }
}
