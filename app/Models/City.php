<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class City extends Model
{
    public function shop(){
    	return $this->hasMany(Shop::class, 'city_id');
    }

}
