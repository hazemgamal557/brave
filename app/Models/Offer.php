<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Offer extends Model
{
    protected $fillable = [
        "product_name", 
        "price_before", 
        "price_after", 
        
        "image", 
        "shop_id"
    ];
    public function getImageAttribute()
    {
        if (empty($this->attributes['image'])) {
            return asset('storage/offer_temp/product.png');
        }else{
            return asset("storage/{$this->attributes['image']}");
        }
    }

    public function shop()
    {
    	return $this->belongsTo(Shop::class);
    }
    public function favourite()
    {
    	return $this->hasMany(Favourite::class, 'offer_id');
    }

    public function coupon()
    {
    	return $this->belongsToMany(User::class,
    		'coupon_user', 
    		'offer_id', 
    		'user_id'
    	);
    }

}
