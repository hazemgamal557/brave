<?php

namespace App\Http\Controllers;

use App\Models\Offer;
use App\Models\Favourite;
use App\Models\Shop;
use App\Services\FileManager;
use App\Http\Resources\OfferResource;
use App\Http\Requests\OfferRequest;

use Illuminate\Http\Request;

class OfferController extends Controller
{
    public function add(OfferRequest $request, FileManager $fileManager)
    {
        $shop = \Auth::user()->shop;

        if($shop->verified == 0) {
            return response()->json([
                'message' => __('messages.your-shop-is-not-verified-yet')
            ], 200);
        }
        
        
        $offer = new Offer;
        if ($request->hasFile('image')  && request()->file('image')->isValid()) {
            $offer->image = $fileManager->uploadOfferImage(request()->file('image'));
        }
        else {
            $offer->image = '';
        }

        $offer->product_name = $request->product_name;
        $offer->price_before = $request->price_before;
        $offer->price_after = $request->price_after;
        
        $offer->shop_id = $shop->id;
        $offer->save();

        return response()->json([
            'message' => __('messages.offer-has-been-added')
        ], 201);
    }

    public function update(OfferRequest $request, FileManager $fileManager, $id)
    {
        $shop = \Auth::user()->shop;
        $offer = Offer::findOrFail($id);

        if (!(\Auth::user()->tokenCan('server:update')  || $offer->shop_id == $shop->id)) {
            return response()->json([
                'message' => __('messages.you-are-not-authenticated')
            ], 401);
        }

        if ($request->hasFile('image') && request()->file('image')->isValid()) {
            $offer->image = $fileManager->uploadOfferImage(request()->file('image'));
        }
      

        $offer->product_name = $request->product_name;
        $offer->price_before = $request->price_before;
        $offer->price_after = $request->price_after;
        $offer->save();

        return response()->json([
            'message' => __('messages.offer-has-been-updated')
        ], 201);
    }

    public function index(Request $request)
    {
        $offerQuery = Offer::query();

        if (!empty($request->shop)) {
            $offerQuery->where('shop_id', $request->shop);
        }

        $authGuard = \Auth::guard('sanctum');

        if ($authGuard->check()) {
            $offerQuery->leftJoin('favourites', function ($join) use ($authGuard) {
                $join->on('offers.id', '=', 'favourites.offer_id')
                    ->where('favourites.user_id', $authGuard->id());
            })
            ->selectRaw('count(DISTINCT favourites.user_id) AS is_favourite, offers.*')
            ->groupBy('offers.id');
        }

        $records = $offerQuery->orderBy('id', 'desc')
            ->paginate($request->query('limit', 10));

        return response()->json(OfferResource::collection($records), 200);
    }

    public function myOffers()
    {  
        $shop = \Auth::user()->shop;

        $records = Offer::where('shop_id', $shop->id)->orderBy('id', 'desc')
            ->paginate(10);

        return response()->json(OfferResource::collection($records), 200);
    }
    
    public function show(Offer $id)
    {  
        
        return new OfferResource($id);
    }

    public function delete($id)
    {
        $shop = \Auth::user()->shop;
        $offer = Offer::findOrFail($id);

        if (!(\Auth::user()->tokenCan('server:update') || $offer->shop_id == $shop->id)) {
            return response()->json([
                'message' => __('messages.you-are-not-authenticated')
            ], 401);
        }

        $offer->delete();

        return response()->json([
            'message' => __('messages.offer-has-been-deleted')
        ], 201);
    }
}
