<?php

namespace App\Http\Controllers;

use App\Http\Resources\ShopResource;
use App\Http\Resources\ShopCollection;
use Illuminate\Http\Request;
use App\Http\Requests\ShopRequest;
use App\Http\Requests\SearchRequest;
use App\Services\FileManager;
use App\Models\Shop;
use App\Models\City;
use App\Models\Category;
use App\Models\Offer;
use App\Models\User;

class ShopController extends Controller
{
    public function index(SearchRequest $request)
    {
        $authGuard = \Auth::guard('sanctum');
        $isAuthenticatedWithLocation = $authGuard->check() && $authGuard->user()->latitude && $authGuard->user()->longitude;

        $shopQuery = Shop::query();

        $selectRaw = 'shops.*';
        if ($isAuthenticatedWithLocation) {
            $selectRaw .= ", (6371 * acos(cos(radians({$authGuard->user()->latitude})) * cos(radians(shops.latitude)) *
            cos(radians(shops.longitude) - radians({$authGuard->user()->longitude})) + sin(radians({$authGuard->user()->latitude})) *
            sin(radians(shops.latitude)))) AS distance";
        }

        if (!empty($request->input)) {
            $shopQuery->where('name', 'like',  $request->input . '%');
        }

        if (!empty($request->city)) {
            $shopQuery->where('city_id', $request->city);
        }

        if (!empty($request->category)) {
            $shopQuery->where('category_id', $request->category);
        }

        if (!empty($request->sort == 'rating')) {
            $shopQuery->orderBy('rating', 'DESC');
        }

        if ($isAuthenticatedWithLocation && !empty($request->sort == 'location')) {
            $shopQuery->orderBy('distance', 'ASC');
        }

        $records = $shopQuery->selectRaw($selectRaw)
            ->where('verified', 1)
            ->paginate($request->query('limit', 10));

        $shops =  ShopResource::collection($records);
        return response()->json($shops, 200);
    }

    public function store(ShopRequest $request, FileManager $fileManager)
    {
        $user = \Auth::user();

        $cityName = City::where('name', $request->city)->first();
        $categoryName = Category::where('name', $request->category)->first();
        if ($user->shop()->count() == 1) {
            return response()->json([
                'message' => __('messages.you-already-created-a-shop')
            ], 401);
        }

        $shop = new Shop;

        if ($request->hasFile('image') && request()->file('image')->isValid()) {
            $shop->image = $fileManager->uploadShopImage(request()->file('image'));
        } else {
            $shop->image = '';
        }

        $shop->name = $request->name;
        $shop->bio = $request->bio;
        $shop->latitude = $request->latitude;
        $shop->longitude = $request->longitude;
        $shop->code = $request->code;
        $shop->coupon_limit = $request->coupon_limit;
        $shop->city_id = $cityName->id;
        $shop->category_id = $categoryName->id;
        $shop->user_id = \Auth::user()->id;
        $shop->save();

        $user->role = 'shop';
        $user->update();

        return response()->json([
            'message' => __('messages.shop-has-been-created')
        ], 201);
    }

    public function show(Shop $shop)
    {
        if ($shop->verified == 0) {
            return response()->json([
                'message' => __('messages.shop-is-not-verified')
            ], 401);
        }

        return new ShopResource($shop);
    }

    public function update(Request $request, $id, FileManager $fileManager)
    {
        $request->validate([
            'coupon_limit' => 'required|numeric',
            'bio' => 'required',
            'code' => 'required|numeric|digits:4',
        ]);

        $shop = Shop::findOrFail($id);

        if (!($shop->user_id == \Auth::user()->id || \Auth::user()->tokenCan('server:update'))) {
            return response()->json([
                'message' => __('messages.you-are-not-authenticated')
            ], 401);
        }

        if ($request->hasFile('image')  && request()->file('image')->isValid()) {
            $shop->image = $fileManager->uploadShopImage(request()->file('image'));
        }
        $shop->bio = $request->bio;
        $shop->coupon_limit = $request->coupon_limit;
        $shop->code = $request->code;
        $shop->update();

        return response()->json([
            'message' => __('messages.shop-has-been-updated')
        ], 201);
    }

    public function destroy($id)
    {
        $shop = Shop::findOrFail($id);

        if (!($shop->user_id == \Auth::user()->id || \Auth::user()->tokenCan('server:update'))) {
            return response()->json([
                'message' => __('messages.you-are-not-authenticated')
            ], 401);
        }

        $shop->delete();


        $user =  $shop->user;
        if($user->role == 'shop'){
        $user->role = 'user';
        $user->save();
        }
        return response()->json([
            'message' => __('messages.shop-has-been-deleted')
        ], 201);
    }

    // public function verify($id)
    // {
    //     $shop = Shop::findOrFail($id);

    //     $shop->verified = 1;
    //     $shop->update();

    //     return response()->json([
    //         'message' => __('messages.shop-has-been-verified')
    //     ], 200);
    // }




    //    public function showShopCoupon(){
    //        $shop = Auth::user()->shop()->first();

    //        $offers = Offer::where('shop_id', $shop->id)->get();

    //        $result = CodeResource::collection($offers);
    //        return response()->json($result, 200);
    //    }
}
