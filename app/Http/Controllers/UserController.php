<?php

namespace App\Http\Controllers;
Use App\Models\User;
use Illuminate\Http\Request;
use App\Http\Resources\UserResource;

class UserController extends Controller
{
    public function index()
    {
        $users = UserResource::collection(User::paginate(10));
        return response()->json($users, 200);
    }

    public function show()
    {
        $authUser = \Auth::user();
        $auth= new UserResource($authUser);
        return response()->json($auth);
    }

    public function update(Request $request)
    {   
        $id = \Auth::user()->id;
        $user = User::findOrFail($id);

        $user->name = $request->name;
        $user->save();

        return response()->json([
          'message' => __('messages.name-has-been-updated')
      ], 201);
    }

    public function destroy($id)
    {
        $user = User::findOrFail($id);
        $user->delete();

        return response()->json([
            'message' => __('messages.user-has-been-deleted')
        ], 201);
    }




    public function setLang(Request $request)
    {
        $data = $request->validate([
            'lang' => ['required', 'regex:/^(ar|en)$/'],
        ]);

        $authUser = \Auth::user();

        $authUser->update([
            'lang' => $data['lang']
        ]);

        return response()->json([
            'message' => __('messages.updated-sucessfully')
        ], 201);
    }


    public function setDeviceId(Request $request)
    {
        $data = $request->validate([
            'device_id'  => 'required|string'
        ]);

        User::where('device_id', $data['device_id'])
            ->where('id', '!=', \Auth::id())
            ->update(['device_id' => null]);

        $authUser = \Auth::user();
        $authUser->device_id = $data['device_id'];
        $authUser->save();

        return response()->json([
            'message' => __('messages.updated-sucessfully')
        ], 201);
    }


    public function setLocation(Request $request)
    {
        $data = $request->validate([
            'latitude'  => 'required|numeric',
            'longitude'  => 'required|numeric'
        ]);
        // return \Auth::user();
        $user = \Auth::user();
        $user->latitude = $data['latitude'];
        $user->longitude = $data['longitude'];
        $user->update();

        return response()->json([
            'message' => __('messages.updated-sucessfully')
        ],201);

    }
 }

