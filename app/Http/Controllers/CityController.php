<?php

namespace App\Http\Controllers;

use App\Models\City;
use Illuminate\Http\Request;
use App\Http\Requests\CityRequest;
use App\Http\Resources\CityResource;
use App\Http\Controllers\Controller;

class CityController extends Controller
{
  

    public function index(Request $request)
    {
        $records = City::orderBy('id', 'desc')
            ->paginate($request->query('limit', 10));


        $cities = CityResource::collection($records);
        return response()->json($cities);
    }

  
}
