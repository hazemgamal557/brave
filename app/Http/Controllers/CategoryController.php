<?php

namespace App\Http\Controllers;
use App\Services\FileManager;
use App\Http\Requests\CategoryRequest;
use App\Models\Category;
use App\Http\Resources\CategoryResource;
use Illuminate\Http\Request;

class CategoryController extends Controller
{
    public function index(Request $request)
    {
        $records = Category::orderBy('id', 'desc')
            ->paginate(
                $request->query('limit', 10)
            );
        $categories = CategoryResource::collection($records);
        
        return response()->json($categories);
    }

   
    public function show(Category $category)
    {
        return new CategoryResource($category);
    }

   
}
