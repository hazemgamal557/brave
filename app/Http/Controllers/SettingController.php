<?php

namespace App\Http\Controllers;
use App\Http\Resources\SettingResource;
use App\Models\Setting;
use Illuminate\Http\Request;
class SettingController extends Controller
{
    public function retriveSocialMedia(Setting $setting)
    {
        $facebook = Setting::where('id',  '7')->first();
        $twitter = Setting::where('id',  '8')->first();
        $instagram = Setting::where('id',  '9')->first();
        $youtube = Setting::where('id',  '10')->first();
        $whatsapp = Setting::where('id',  '11')->first();
        $snapchat = Setting::where('id',  '12')->first();
        $phone = Setting::where('id',  '13')->first();
        $gmail = Setting::where('id',  '14')->first();

        return response()->json(
            [
            "facebook" => $facebook->content,
            "twitter" => $twitter->content,
            "instagram" => $instagram->content,
            "youtube" => $youtube->content,
            "whatsapp" => $whatsapp->content,
            "snapchat" => $snapchat->content,
            "phone" => $phone->content,
            "gmail" => $gmail->content
            ]
             
            
        );
    }
    
    public function getPage(string $key)
    {
        $key = "page_{$key}_" . app()->getLocale();

        $page = Setting::where('key', $key)
            ->firstOrFail()
            ->content;

        return response()->json([
            'content' => $page
        ]);
    }
    
   

}
