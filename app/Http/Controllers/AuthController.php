<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Traits\UpdatePhoneTrait;
use App\Http\Resources\UserResource as UserResource;
use App\Models\User;
use App\Mail\SendConfirmCode;
use Illuminate\Http\Request;
use Illuminate\Mail\Mailable;
use Illuminate\Validation\Rule;
use Illuminate\Support\Facades\Mail;

class AuthController extends Controller
{
    use UpdatePhoneTrait;

    public function signupWithEmail(Request $request)
    {
        $data = $request->validate([
            'email' => [
                'required',
                'email',
                Rule::unique('users')->where('email_verified_at', '!=', null),
            ],
            'name' => 'required|string',
            'password' => [
                'required',
                'string',
                'min:6',
            ],
            'phone' => 'required|string',
        ]);
        User::where('email', $data['email'])
            ->where('email_verified_at', null)
            ->delete();

        $data['password'] = \Hash::make($data['password']);

        $user = User::create($data);
        $user->email_verified_at = now();
        $user->save();

        return response()->json([
            'user' => new UserResource(User::find($user->id)),
            'token' => $user->createToken('my-lovely-token', ['server:show'])->plainTextToken,
            'message' => __('messages.user-activated'),
        ], 201);

    }


    public function login(Request $request)
    {
        $data = $request->validate([
            'email' => 'required|email',
            'password' => 'required',
        ]);

        $user = User::where('email', $data['email'])->first();
        if (is_null($user) || !\Hash::check($data['password'], $user->password)) {
            return response()->json([
                'message' => 'sorry your data is not true',
            ], 401);
        }
        return response()->json([
            'user' => new UserResource($user),
            'token' => $user->createToken('my-lovely-token', ['server:show'])->plainTextToken,
            'message' => 'you are login success',
        ], 201);

    }

    public function logout(Request $request)
    {
        $request->user()->currentAccessToken()->delete();

        return response()->json([
            'message' => __('messages.logged-out'),
        ]);
    }

    public function sendForgetPasswordEmail(Request $request)
    {
        $data = $request->validate([
            'email' => 'required|email',
        ]);
        $email = $data['email'];
        $user = User::where('email', $email)
            ->where('email_verified_at', '!=', null)
            ->first();
        if(! $user){
            return response()->json([
                'message' => 'sorry your email is not exisits'
            ], 401);
        }
        $password = rand(10000, 10000000);
        $user->password = \Hash::make($password);
        $user->save();
        return response()->json([
            'password' => $password,
            'message'  => 'your password is sent'
        ], 201);
    }
    public function updateUserPassword(Request $request)
    {
        $data = $request->validate([
            'password' => [
                'required',
                'string',
                'min:6',
            ],
        ]);
        $password = $data['password'];
        $user = \Auth::user();
        $user->password = \Hash::make($password);
        $user->save();
        return response()->json([
            'message' => 'your password changed success'
        ], 201);
    }

    protected function generateConfirmationCode(): string
    {
        return (string) 111111;
    }

}
