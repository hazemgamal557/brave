<?php

namespace App\Http\Controllers;
use App\Models\User;
use Illuminate\Http\Request;
use App\Http\Requests\ResetPasswordRequest;
use Illuminate\Support\Facades\Password;

class ForgetPasswordController extends Controller
{
    public function forgetPassword(Request $request)
    {
        $data = $request->validate([
            'email' => 'required|email'
        ]);
        Password::sendResetLink($data);

        return $this->respondWithMessage('Reset password link sent on your email id.');
    }
}
