<?php

namespace App\Http\Controllers;

use App\Models\Offer;
use App\Models\Favourite;
use App\Http\Resources\FavouriteResource;
use Auth;

class FavouriteController extends Controller
{
    public function add($id)
    {
        $offer = Offer::findOrFail($id);

        $check = Favourite::where([
            ['user_id', '=', \Auth::user()->id],
            ['offer_id', '=', $id]
        ])->first();

        if (!empty($check)) {
                $check->delete();
            return response([
                'message' => __('messages.item-has-been-deleted')
            ], 200);
        }

        $favourite = new Favourite;
        $favourite->user_id = \Auth::user()->id;
        $favourite->offer_id = $id;
        $favourite->save();

        return response([
            'message' => __('messages.item-has-been-added-to-favourites')
        ], 200);
    }

    public function show()
    {
        $records = Favourite::where('user_id', '=', Auth::user()->id)->get();
        $favourites = FavouriteResource::collection($records);

        return response()->json($favourites);
    }

}
