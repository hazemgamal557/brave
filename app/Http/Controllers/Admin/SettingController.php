<?php

namespace App\Http\Controllers\Admin;

use App\Http\Resources\SettingResource;
use App\Models\Setting;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class SettingController extends Controller
{
    public function update(Request $request, $id)
    {
        $setting = Setting::findOrFail($id);

        $setting->content = $request->content;
        $setting->update();

        return response()->json([
            'message' => __('messages.setting-has-been-updated')
        ], 201);
    }
    public function retriveSocialMedia(Setting $setting)
    {
        $settings = Setting::where('id',  '7')
        ->orWhere('id', '8')
        ->orWhere('id', '9')
        ->orWhere('id', '10')
        ->orWhere('id', '11')
        ->orWhere('id', '12')
        ->orWhere('id', '13')
        ->orWhere('id', '14')->get();

        return response()->json($settings);
        
    }
  

    public function getPage(string $key)
    {
        $key = "page_{$key}_" . app()->getLocale();

        $page = Setting::where('key', $key)
            ->firstOrFail()
            ->content;

        return response()->json([
            'content' => $page
        ]);
    }
    public function updatePage(Request $request, string $key)
    {
        $data = $request->validate(['content' => 'required|string']);

        $page = Setting::where('key', "page_{$key}_{$request->query('lang')}")
            ->update(['content' => $data['content']]);

        return response()->json([
            'message' => 'page has been updated successfully'
        ]);
    }
}
