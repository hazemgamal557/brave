<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Resources\ShopResource;
use App\Http\Resources\ShopCollection;
use Illuminate\Http\Request;
use App\Http\Requests\ShopRequest;
use App\Http\Requests\SearchRequest;
use App\Services\FileManager;
use App\Models\Shop;
use App\Models\City;
use App\Models\Category;
use App\Models\Offer;
use App\Models\User;

class ShopController extends Controller
{
    public function index(Request $request)
    {
        // $shops =  ShopResource::collection($records);
        $input = $request->validate([
            'id' => 'numeric|exists:products,id',
            'q' => 'string',
            'limit' => 'integer|min:1',
            'selected' => 'string',
            'category_id' => 'string'
        ]);

        $records = Shop::with('user', 'city', 'category')->orderBy('id', 'desc');

        if (!empty($input['q'])) {
                $records->where('name', 'LIKE', "%{$input['q']}%");
        }

        // if (!empty($input['category_id'])) {
        //     $records->where('category_id', $input['category_id']);
        // }


        if (!empty($input['category_id'])) {
            $records->whereHas('category', function ($query) use ($input) {
                $query->where('name', 'LIKE', "%{$input['category_id']}%");
            });
        }

        if (!empty($input['selected'])) {
            if ($input['selected'] == 1) {
                $records->where('verified', 1);
            } else {
                $records->where('verified', 0);
            }
        }

        return $records->paginate(
            $request->query('limit', 10)
        );    }

    public function indexAll(Request $request)
    {
        $records = Shop::all();
        // $shops =  ShopResource::collection($records);
        return response()->json($records, 200);
    }

    public function unverified(Request $request)
    {
        $records = Shop::with('user', 'city', 'category')->where('verified', 0)->orderBy('id', 'desc')->paginate(10);
        // $shops =  ShopResource::collection($records);
        return response()->json($records, 200);
    }


    public function destroy($id)
    {
        $shop = Shop::findOrFail($id);
        $shop->delete();


        $user =  $shop->user;
        $user->role = 'user';
        $user->save();


        return response()->json([
            'message' => __('messages.shop-has-been-deleted')
        ], 201);
    }

    public function verify($id)
    {
        $shop = Shop::findOrFail($id);

        if($shop->verified == 0){
            $shop->verified = 1;
            $shop->update();
            return response()->json([
                'message' => __('messages.shop-has-been-verified')
            ], 200);
        }
        else if($shop->verified == 1){
            $shop->verified = 0;
            $shop->update(); 
            return response()->json([
                'message' => 'تم تعطيل المتجر بنجاح'

            ], 200);
        }
        
        
    }
  
    public function search(){

        if ($search = \Request::get('q')) {
            // $search = \Request::get('q');
            $shops = Shop::with('user', 'city', 'category')->orderBy('id', 'desc')->where(function($query) use ($search){
                $query->where('name','LIKE',"$search%");
            })->paginate(10);
        }else{
            $shops = Shop::with('user', 'city', 'category')->orderBy('id', 'desc')->paginate(10);
        }

        return $shops;

    }


    //    public function showShopCoupon(){
    //        $shop = Auth::user()->shop()->first();

    //        $offers = Offer::where('shop_id', $shop->id)->get();

    //        $result = CodeResource::collection($offers);
    //        return response()->json($result, 200);
    //    }
}
