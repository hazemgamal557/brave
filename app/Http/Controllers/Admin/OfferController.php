<?php

namespace App\Http\Controllers\Admin;

use App\Models\Offer;
use App\Models\Favourite;
use App\Services\FileManager;
use App\Http\Resources\OfferResource;
use App\Http\Requests\OfferRequest;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;

class OfferController extends Controller
{

    public function index(Request $request)
    {
        $records = Offer::with('shop')->orderBy('id', 'desc')->paginate(10);
        return response()->json($records, 200);
    }

    public function showOffer($id){
        $records = Offer::with('shop')->where('shop_id', $id)->orderBy('id', 'desc')
        ->paginate(10);

    return response()->json($records, 200);
    }

    public function delete($id)
    {
        $offer = Offer::findOrFail($id);

        $offer->delete();

        return response()->json([
            'message' => __('messages.offer-has-been-deleted')
        ], 201);
    }
}
