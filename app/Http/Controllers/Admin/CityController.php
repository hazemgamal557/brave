<?php

namespace App\Http\Controllers\Admin;

use App\Models\City;
use Illuminate\Http\Request;
use App\Http\Requests\CityRequest;
use App\Http\Resources\CityResource;
use App\Http\Controllers\Controller;

class CityController extends Controller
{
    public function store(CityRequest $request)
    {
        $city = new City;
        $city->name = $request->name;
        $city->save();

        return response()->json([
            'message' => __('messages.city-has-been-added')
        ], 201);
    }

    public function update(CityRequest $request, $id)
    {
        $city = City::findOrFail($id);
        $city->name = $request->name;

        $city->update();

        return response()->json([
            'message' => __('messages.city-has-been-updated')
        ], 201);
    }

    public function withShop(Request $request)
    {
        return City::withCount(['shop'])->get();

    }

    public function index(Request $request)
    {
        $records = City::orderBy('id', 'desc')
            ->paginate($request->query('limit', 10));


        return response()->json($records, 201);
    }

    public function destroy($id)
    {
        $city = City::findOrFail($id);

        $city->delete();

        return response()->json([
            'message' => __('messages.city-has-been-deleted')
        ], 201);
    }

}
