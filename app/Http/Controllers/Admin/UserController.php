<?php

namespace App\Http\Controllers\Admin;
Use App\Models\User;
Use App\Models\Shop;
Use App\Models\Offer;
Use App\Models\Category;
Use App\Models\City;
use Illuminate\Http\Request;
use App\Http\Resources\UserResource;
use App\Http\Controllers\Controller;

class UserController extends Controller
{
    public function index()
    {
        $users = User::orderBy('id', 'desc')->paginate(10);
        return response()->json($users, 200);
    }

    public function count()
    {
        $shops = Shop::count();
        $offers = Offer::count();
        $categories = Category::count();
        $users = User::count();
        $cities = City::count();
        return response()->json([
            'users'=> $users,
            'shops'=> $shops,
            'offers'=> $offers,
            'categories'=> $categories,
            'cities' => $cities
         ]);
    }

    public function ratio()
    {
        $shops = User::where('role', 'shop')->count();
        $users = User::where('role', 'user')->count();
        
        return response()->json([
            'users'=> $users,
            'shops'=> $shops,
           ]);
    }
    
    public function destroy($id)
    {
        $user = User::findOrFail($id);
        $user->delete();

        return response()->json([
            'message' => __('messages.user-has-been-deleted')
        ], 201);
    }
    public function search(){

        $search = \Request::get('q');
        $users = User::orderBy('id', 'desc')->where('role', $search)->paginate(10);
       
        return $users;

    }


 }

