<?php

namespace App\Http\Controllers\Admin;

use App\Models\User;
use App\Models\Notification;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Services\NotificationManager;
use App\Http\Resources\NotificationResource;

class NotificationController extends Controller
{
    /**
     * send notification to all|specific user
     * 
     * @param int $_GET['page']
     * @param int $_GET['limit']
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */

    public function index(Request $request)
    {

        $notifications = Notification::orderBy('id', 'desc')
            ->paginate(
                $request->query('limit', 10)
            );

        // $notification = NotificationResource::collection($notifications);

        return response()->json($notifications);
    }


    public function send(Request $request, NotificationManager $notificationManager)
    {
        $data = $request->validate([
            'message_ar' => 'required|string',
            'message_en' => 'required|string'
        ]);



        $notification = new Notification;
        $notification->message_ar = $request->message_ar;
        $notification->message_en = $request->message_en;
        $notification->save();



        $usersQuery = User::select(['id', 'device_id', 'lang']);
        
        
        $users = $usersQuery->get();

        foreach(['ar', 'en'] as $lang) {
            $tokens = $users->whereNotNull('device_id')
                ->where('lang', $lang)
                ->pluck('device_id')
                ->toArray();

            $notificationManager->send(
                $tokens,
                $data["message_{$lang}"]
            );
        }

       

        return response()->json([
            'message' => 'the notifications have been sent successfully'
        ]);
    }


    public function delete($id){
        $notification = Notification::findOrFail($id);
        $notification->delete();

        return response()->json([
            'message'=> 'notification has been deleted'
        ]);
    }
   
}
