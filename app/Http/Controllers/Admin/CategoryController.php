<?php

namespace App\Http\Controllers\Admin;
use App\Services\FileManager;
use App\Http\Requests\CategoryRequest;
use App\Models\Category;
use App\Http\Resources\CategoryResource;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class CategoryController extends Controller
{
    public function index(Request $request)
    {
        $records = Category::orderBy('id', 'desc')
            ->paginate(
                $request->query('limit', 10)
            );
        return $records;
    }


    public function indexAll(Request $request)
    {
        $records = Category::all();
        return $records;
    }

    public function store(CategoryRequest $request, FileManager $fileManager)
    {
        $category = new Category;
        $category->name = $request->input('name');

        if ($request->hasFile('image')  && request()->file('image')->isValid()) {
            $category->image = $fileManager->uploadCategoryImage(request()->file('image'));
        }
        else {
            $category->image = '';
        }

        $category->save();

        return response()->json([
            'message' => __('messages.category-has-been-added')
        ], 201);
    }

    public function show(Category $category)
    {
        return new CategoryResource($category);
    }

    public function update(CategoryRequest $request, FileManager $fileManager, int $id)
    {
        $category = Category::findOrFail($id);
        $category->name = $request->input('name');

        if ($request->hasFile('image')  && request()->file('image')->isValid()) {
            $category->image = $fileManager->uploadCategoryImage(request()->file('image'));
        }
       

        $category->update();

        return response()->json([
            'message' => __('messages.category-has-been-updated')
        ], 200);
    }

    public function destroy(Category $category)
    {
        $category =  $category->delete();

        return response()->json([
            'message' => __('messages.category-has-been-deleted')
        ], 201);
    }
}
