<?php

namespace App\Http\Controllers;

use App\Models\User;
use App\Models\Notification;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Services\NotificationManager;
use App\Http\Resources\NotificationResource;

class NotificationController extends Controller
{
    /**
     * send notification to all|specific user
     * 
     * @param int $_GET['page']
     * @param int $_GET['limit']
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
  

    public function index(Request $request)
    {

        $notifications = Notification::orderBy('id', 'desc')
            ->paginate(
                $request->query('limit', 10)
            );

        $notification = NotificationResource::collection($notifications);

        return response()->json($notifications);
    }
}
