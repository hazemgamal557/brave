<?php

namespace App\Http\Controllers;
use Carbon\Carbon;

use Auth;
use App\Models\Shop;
use App\Models\Offer;
use App\Models\User;
use App\Models\Coupon;
use App\Http\Resources\CouponResource;

use Illuminate\Http\Request;

class CouponController extends Controller
{
    public function coupon(Request $request){
        $coupon = $request->coupon;

        $shop = Shop::findOrFail($request->shop_id);

        $check = Shop::where('id', $shop->id)->where('code', $coupon)->count();
        if($check == 0){
            return response()->json([
                'message' => __('messages.coupon-is-not-right')
                ], 403);
        }
        $count = Coupon::where('shop_id', $shop->id)->where('user_id', Auth::user()->id)->whereDate('created_at',  Carbon::today())->count();
        
        if($count >= $shop->coupon_limit){
            return response()->json([
            'message' => __('messages.coupon-has-been-used-maximum-times')
            ], 403);
        }

     
       
                  

        $coupon = Coupon::create( ['created_at' => now()->toDateTimeString(), 'coupon'=>$shop->code, 'shop_id'=>$shop->id, 'user_id' => auth::user()->id]);
        // return ($coupon);
        // return response($count);


        return response()->json([
            'coupon_id'=> $coupon->id,
            'message' => __('messages.coupon-has-been-used-successfully')
        ], 201);
    }


    public function showCoupons(){
        
        $user = User::findOrFail(Auth::user()->id);

        $data =  Coupon::where('user_id', $user->id)->get();   

        $return =  CouponResource::collection($data);
        return response()->json($return, 200);

    }


    public function rate(Request $request){
        $rating = $request->rate;
        $id = $request->coupon_id;
        $user = User::where('id', Auth::user()->id)->get();
        
        $request->validate([
            'rating' => 'required|max:5|numeric',
            // 'id' => 'required|unique:coupon_user'

        ]);
        $coupon = Coupon::findOrFail($id);


        $count = Coupon::where('user_id', auth::user()->id)->where('id', $id)->where('rating', null)->count();
        if($count == 0){
            return response()->json([
                'message' => __('messages.you-are-not-authenticated')
            ], 403);
        } 
        $coupon->rating = $request->rating;
        $coupon->update();

        $usersRated = Coupon::where('shop_id', $coupon->shop_id)->where('rating', '!=' , null)->count();
        $rateSum = Coupon::where('shop_id', $coupon->shop_id)->sum('rating'); 

        $finalRate = $rateSum / $usersRated;

        $shop = Shop::findOrFail($coupon->shop_id);
        $shop->rating = $finalRate;
        $shop->update();

        return response()->json([
            'message' => __('messages.shop-has-been-rated')
        ], 201);
    }
}



   // $userCoupon =  $offer->coupon()->where('coupon', $offer->code)->where('user_id', Auth::user()->id)->count();
        // if($userCoupon == 1){
        //     return response()->json([
        //         'message' => __('messages.you-have-already-used-the-coupon')
        //     ], 403);
        // }


        // $count = $offer->coupon()->where('offer_id', $id)->count();
        // if($count >= $offer->coupon_limit){
        //     return response()->json([
        //         'message' => __('messages.coupon-has-been-used-maximum-times')
        //     ], 403);
        // }
            
