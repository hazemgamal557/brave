<?php

namespace App\Http\Controllers\Traits;

use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use Illuminate\Support\Facades\Hash;

trait UpdatePhoneTrait
{
    /**
     * send the given sms $message to the given $phone
     *
     * @return void
     */
    // abstract protected function sendSMS(string $phone, string $message) : void;

    /**
     * generate the confirm_code field
     *
     * @return string
     */
    abstract protected function generateConfirmationCode() : string;

    // public function sendUpdateEmailAndConfrimCode(Request $request)
    // {
    //     $data = $request->validate([
    //         'email' => [
    //             'required',
    //             'email',
    //             Rule::unique('users')->where('email_verified_at', null)->ignore(\Auth::id())
    //         ]
    //     ]);
    //     $user = \Auth::user();
    //     $email = $data['email'];
    //     $confirmCode = $this->generateConfirmationCode();
    //     $user->confirm_code = $confirmCode;
    //     $user->request_confirm_code_date = now();
    //     $user->request_new_email = $email;
    //     $user->save();
    //     return response()->json([
    //         'ttl' => User::CONFIRM_CODE_EXPIRES_IN_SEC,
    //         'message' => __('messages.confirm-code-sent')
    //     ], 201);

    // }
    // public function checkConfirmCodeAndUpdateEmail(Request $request)
    // {
    //     $data = $request->validate([
    //         'confirm_code' => 'required'
    //     ]);
    //     $user = \Auth::user();
    //     if(! $user->isConfirmCodeActive()){
    //         return response()->json([
    //             'message' => __('messages.confirm-code-expired')
    //         ], 401);
    //     }
    //     if($user->confirm_code != $data['confirm_code'])
    //     {
    //         $user->confirm_code = null;
    //         $user->save();
    //         return response()->json([
    //             'message' => __('messages.confirm-code-not-valid')
    //         ], 401);
    //     }
    //     $user->confirm_code = null;
    //     $user->request_confirm_code_date = null;
    //     $user->email = $user->request_new_email;
    //     $user->request_new_email = null;
    //     $user->save();
    //     return response()->json([
    //         'message' => 'update email success'
    //     ], 201);
    // }

}
