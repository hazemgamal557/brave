<?php

namespace App\Http\Middleware;
use Auth;
use Closure;

class isShop
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
        {
            
            $user = auth::user();

        if(Auth::user()->role == 'shop' || $user->tokenCan('server:update')){
          
            return $next($request);
            
        }else{
            return response()->json([
                'message' => __('messages.you-are-not-authenticated')
            ], 401);

        }
    }
}
