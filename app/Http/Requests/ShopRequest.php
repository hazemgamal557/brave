<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ShopRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required',
            'bio' => 'required',
            // 'address' => 'required',
            'image' => 'image|mimes:png,jpg,jpeg,bmp',
            'city' => 'required|string',
            'category' => 'required|string',
            'code' => 'required|integer',
            'coupon_limit' => 'required|integer',
            'lat' => 'numeric',
            'long' => 'numeric',
            'rating' => 'numeric',
            
            
        ];
    }
}
