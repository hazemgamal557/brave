<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class SearchRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            // 'input' => 'string',
            'city' => 'numeric|exists:cities,id',
            'category' => 'numeric|exists:categories,id'
        ];
    }

    public function messages()
    {
        return [
            'input.string' => 'Sorry, the shop should be text',
            'city.exists' => 'Sorry, the city does not exist',
            'category.exists' => 'Sorry, the category does not exist'
        ];
    }
}
