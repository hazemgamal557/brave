<?php

namespace App\Http\Resources;
use App\Models\Shop;
use Illuminate\Http\Resources\Json\JsonResource;

class CouponResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $shop = Shop::find($this->shop_id);
        return [
            'id' => $this->id,
            'shop_name' => $shop->name,
            'rating' => $this->rating,
            'created_at' => $this->created_at,

        ];
    }
}
