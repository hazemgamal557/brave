<?php

namespace App\Http\Resources;
use App\Models\Category;
use App\Models\City;

use Illuminate\Http\Resources\Json\JsonResource;

class ShopResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {

        $resource = [
            'id' => $this->id,
            'name' => $this->name,
            'bio' => $this->bio,
            'latitude' => $this->latitude,
            'longitude' => $this->longitude,
            'rate' => round($this->rating, 1),
            'image' => $this->image,
            'status' => $this->verified,
            'distance' => $this->distance ? number_format ($this->distance , '2' ) : number_format ($this->distance , '2' ),
            'category'=> Category::where('id', $this->category_id)->select('name')->first()['name'],
            'city'=> City::where('id', $this->city_id)->select('name')->first()['name'],
            'navigation'=> 'https://www.google.com/maps/dir/?api=1&origin=Current+Location&destination='.$this->latitude.','.$this->longitude.'&travelmode=driving' ,



        ];


        $guard = \Auth::guard('sanctum');
        // dd($guard->user());
        if ($guard->check() && $guard->user()->shop != null && $this->id == $guard->user()->shop->id) {
            $resource['code'] = $this->code;
            $resource['coupon_limit'] = $this->coupon_limit;
        }

        return $resource;
    }
}
