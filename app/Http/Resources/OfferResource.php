<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class OfferResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'name' => $this->product_name,
            'price_before' => $this->price_before,
            'price_after' => $this->price_after,
            'image' => $this->image,
            'is_favourite' => (bool) $this->is_favourite,
            // 'created_at' => $this->created_at->diffForHumans(),
            'shop'=>  new ShopResource($this->shop),
            ];
    }
}

