<?php

namespace Tests\Feature;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Storage;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class CouponTest extends TestCase
{
    /**
     * A basic feature test example.
     *
     * @return void
     */
    use RefreshDatabase;
    private $admin;

    public function setUp(): void
    {
        parent::setUp();

        $this->admin = \App\Models\Admin::create([
            'name'     => 'أدمن',
            'email'    => 'admin@admin.com',
            'password' => \Hash::make('secret')
        ]);

        $this->user = \App\Models\User::create([
            'name' => 'hazem gamal',
            'email' => 'hazemgamal55@yahoo.com',
            'password' => '123456789'
        ]);


    }
    public function test_add_coupons()
    {
        factory('App\Models\Category')->create();
        factory('App\Models\City')->create();
        $shop =  factory('App\Models\Shop')->create();
        factory('App\Models\Offer')->create();
        $coupons = [
            'rating' => 'rating',
            'coupon' => $shop->code,
            'shop_id' => $shop->id
        ];
        $response = $this->actingAs($this->user)->json('POST', "/api/user/coupons/apply-coupon", $coupons);
        // dd($response);
            $response->assertStatus(201);
            $record = json_decode($response->getContent(), true);
            $this->assertDatabaseHas('coupon_user', [
                'coupon'  => $coupons['coupon'],
            ]);
    }

    public function test_show_coupons()
    {
        factory('App\Models\Category', 3)->create();
        factory('App\Models\City', 3)->create();
        factory('App\Models\Shop', 3)->create();
        factory('App\Models\Offer', 3)->create();
        factory('App\Models\Coupon', 3)->create();

        $response = $this->actingAs($this->user)
        ->json('GET', '/api/user/my/coupons');
        $response->assertStatus(200);

        $this->assertCount(3, json_decode($response->getContent()));

        // dd($response);
    }

}
