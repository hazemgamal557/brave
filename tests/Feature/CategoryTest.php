<?php

namespace Tests\Feature;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Storage;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class CategoryTest extends TestCase
{
    /**
     * A basic feature test example.
     *
     * @return void
     */
 
    use RefreshDatabase;

    private $admin;

    /**
     * create admin to perform our tests
     */
    public function setUp(): void
    {
        parent::setUp();

        $this->admin = \App\Models\Admin::create([
            'name'     => 'أدمن',
            'email'    => 'admin@admin.com',
            'password' => \Hash::make('secret')
        ]);

    
    }

    public function test_retrieve_categories(){

        factory('App\Models\Category', 3)->create();

        $response = $this->actingAs($this->admin)
            ->json('GET', '/api/user/category?limit=2');

            // dd(json_decode($response->getContent()));
                 // assert
        $response->assertStatus(200);

        $this->assertCount(2, json_decode($response->getContent()));

    }

    public function test_store_categories()
        {
            // arrange
            $category = [
                'name'  => 'اسم',
                'image' => UploadedFile::fake()->image('0.jpg', 700, 700)->size(300),
            ];
    
            // act
            $response = $this->actingAs($this->admin)
                ->json('POST', '/api/admin/category/add', $category);
            // dd($response);
            // assert
            $response->assertStatus(201);
            
            $record = json_decode($response->getContent(), true);
    
            # assert record created
            $this->assertDatabaseHas('categories', [
                'name' => $category['name'],
            ]);
            //     dd($record);
            // # image exist
            // $image = explode('storage', $record['image'])[1];
            // Storage::assertExists("public{$image}");
    
            // # remove test files
            // Storage::delete("public/{$image}");
        } 

        public function test_update_categories()
            {
                // arrange
                $category = factory('App\Models\Category')->create();
        
                $form = [
                    'name'  => 'اسم',
                    'image' => UploadedFile::fake()->image('0.jpg', 700, 700)->size(300),
                ];
        
                // act
                $response = $this->actingAs($this->admin)
                    ->json('POST', "/api/admin/category/{$category->id}/update", $form);
        
                // assert
                $response->assertStatus(200);
        
                # assert record created
                $this->assertDatabaseHas('categories', [
                    'name'  => 'اسم',
                ]);
            }

            public function test_remove_category()
                {
                    // arrange
                    $category = factory('App\Models\Category')->create();
            
                    
                    // act
                    $response = $this->actingAs($this->admin)
                        ->json('DELETE', "/api/admin/category/{$category->id}/delete");
            
                    // assert
                    $response->assertStatus(201);
            
                    $this->assertDatabaseMissing('categories', [
                        'id'   => $category->id,
                        'name' => $category->name,
                    ]);
                }
}

