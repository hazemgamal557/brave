<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class UserTest extends TestCase
{
    /**
     * A basic feature test example.
     *
     * @return void
     */

    use RefreshDatabase;

    private $user;

    /**
     * create admin to perform our tests
     */
    public function setUp(): void
    {
        parent::setUp();

        $this->admin = \App\Models\Admin::create([
            'name' => 'أدمن',
            'email' => 'admin@admin.com',
            'password' => \Hash::make('secret'),
        ]);

        $this->user = \App\Models\User::create([
            'name' => 'hazem gamal',
            'email' => 'hazemgamal55@yahoo.com',
            'password' => '123456789',

        ]);

    }

    public function test_retrieve_users()
    {

        factory('App\Models\User', 3)->create();

        $response = $this->actingAs($this->admin)
            ->json('GET', '/api/admin/users');

        // assert
        $response->assertStatus(200);
        // dd($response->json());
        $this->assertCount(4, $response->json()['data']);

    }

    public function test_retrieve_single()
    {

        factory('App\Models\User', 3)->create();

        $response = $this->actingAs($this->user)
            ->json('GET', '/api/user/auth/me/');

        // assert
        $response->assertStatus(200);

        $this->assertCount(4, $response->json());

    }

    public function test_update_users()
    {
        // arrange
        $user = factory('App\Models\User')->create();

        $form = [
            'name' => 'Aamen',
            'phone' => '01254881644',
        ];

        // act
        $response = $this->actingAs($this->user)
            ->json('POST', "/api/user/profile/update", $form);

        // assert
        $response->assertStatus(201);

        # assert record created
        $this->assertDatabaseHas('users', [
            'name' => 'Aamen',
        ]);
    }

    public function test_remove_users()
    {
        // arrange
        $user = factory('App\Models\User')->create();

        // act
        $response = $this->actingAs($this->admin)
            ->json('DELETE', "/api/admin/users/{$user->id}/delete");

        // assert
        $response->assertStatus(201);

        $this->assertDatabaseMissing('users', [
            'id' => $user->id,
        ]);
    }
}
