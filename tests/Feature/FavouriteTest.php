<?php

namespace Tests\Feature;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Storage;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class FavouriteTest extends TestCase
{
    /**
     * A basic feature test example.
     *
     * @return void
     */

    use RefreshDatabase;

    private $user;


    /**
     * create admin to perform our tests
     */
    public function setUp(): void
    {
        parent::setUp();

        $this->user = \App\Models\User::create([
            'name'     => 'hazem gamal',
            'email'     => 'hazemgamal55@yahoo.com',
            'password'  => '123456789'
        ]);



    }

    public function test_retrieve_favourites(){
        factory('App\Models\City', 3)->create();
        factory('App\Models\Category', 3)->create();
        factory('App\Models\Shop', 3)->create();
        factory('App\Models\Offer', 3)->create();
        factory('App\Models\Favourite', 3)->create();

        $response = $this->actingAs($this->user)
            ->json('GET', '/api/user/favourites');

                 // assert
        $response->assertStatus(200);

        $this->assertCount(3, json_decode($response->getContent()));

    }

    public function test_store_favourites()
        {
             factory('App\Models\City')->create();
           factory('App\Models\Category')->create();
            factory('App\Models\Shop')->create();
           $offer =  factory('App\Models\Offer')->create();
            // arrange
            $favourtie = [
                // 'offer_id'  => $offer->id,
                'user_id'  => $this->user->id,
            ];

            // act
            $response = $this->actingAs($this->user)
                ->json('POST', "/api/user/offers/{$offer->id}/favourite", $favourtie);

            // assert
            $response->assertStatus(200);

            $record = json_decode($response->getContent(), true);

            // dd($favourtie);
            # assert record created
            $this->assertDatabaseHas('favourites', [
                'user_id' => $favourtie['user_id'],
            ]);

        }


}


