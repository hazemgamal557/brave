<?php

namespace Tests\Feature;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Storage;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class ShopTest extends TestCase
{
    /**
     * A basic feature test example.
     *
     * @return void
     */

     use RefreshDatabase;

     private $admin;

     private $user;

     public function setUp(): void
    {
        parent::setUp();

        $this->admin = \App\Models\Admin::create([
            'name'     => 'أدمن',
            'email'    => 'admin@admin.com',
            'password' => \Hash::make('secret')
        ]);

        $this->user = \App\Models\User::create([
            'name'     => 'hazem gamal',
            'email'     => 'hazemgamal55@yahoo.com',
            'password'  => '123456789'
        ]);


    }

    public function test_retrieve_shops()
    {
        factory('App\Models\Category', 3)->create();
        factory('App\Models\City', 3)->create();
        factory('App\Models\Shop', 3)->create();
        $response = $this->actingAs($this->user)
            ->json('GET', '/api/user/shops?limit=2');

        // dd($response);
            $response->assertStatus(200);

            $this->assertCount(2, json_decode($response->getContent()));
    }


    public function test_update_shops()
    {
        $category =  factory('App\Models\Category')->create();
        $city = factory('App\Models\City')->create();
       $shop =  factory('App\Models\Shop')->create();
       $form = [
           'bio' => 9,
           'code' => 4410,
           'coupon_limit' => 10,
        //    'user_id' => $this->user->id,

       ];
       $response = $this->actingAs($this->user)->json('POST', "/api/shops/{$shop->id}/update", $form);
    //    dd($response);
       $response->assertStatus(201);

       $this->assertDatabaseHas('shops', [
        'bio'  => 9,
    ]);

    }

    public function test_store_shops()
    {
        $category  = factory('App\Models\Category')->create();
        $city      = factory('App\Models\City')->create();
        // $shop      = factory('App\Models\Shop')->create();

        $shop = [
            'image' => UploadedFile::fake()->image('0.jpg', 700, 700)->size(300),
            'name' => 'cat',
           'bio' => 9,
           'latitude' => 9,
           'longitude' => 10,
           'code' => 10,
           'coupon_limit' => 10,
           'city' => $city['name'],
           'category' => $category['name'],
           'user_id' => $this->user->id,
        ];

        $response = $this->actingAs($this->user)->json('POST', "/api/user/shops/add", $shop);
        $response->assertStatus(201);
        $record = json_decode($response->getContent(), true);

        $this->assertDatabaseHas('shops', [
            'name'  => $shop['name'],
        ]);
    }

    public function test_show_shops()
    {
        factory('App\Models\Category')->create();
        factory('App\Models\City')->create();
        $shop =  factory('App\Models\Shop')->create();

        $response = $this->actingAs($this->user)
            ->json('GET', "/api/user/shops/{$shop->id}");
        // dd($response);
            // dd(json_decode($response->getContent(), true));
        $response->assertStatus(200);

    }
    public function test_remove_shops()
    {
        factory('App\Models\Category')->create();
        factory('App\Models\City')->create();
        $shop = factory('App\Models\Shop')->create();

        $response = $this->actingAs($this->admin)
            ->json('DELETE', "/api/shops/{$shop->id}/delete");

            $response->assertStatus(201);

        $this->assertDatabaseMissing('shops', [
            'id'  => $shop['id'],
        ]);

    }


}
