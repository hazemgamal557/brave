<?php

namespace Tests\Feature;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Storage;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class CityTest extends TestCase
{
    /**
     * A basic feature test example.
     *
     * @return void
     */
 
    use RefreshDatabase;

    private $admin;

    /**
     * create admin to perform our tests
     */
    public function setUp(): void
    {
        parent::setUp();

        $this->admin = \App\Models\Admin::create([
            'name'     => 'أدمن',
            'email'    => 'admin@admin.com',
            'password' => \Hash::make('secret')
        ]);

    
    }

    public function test_retrieve_cities(){

        factory('App\Models\City', 3)->create();

        $response = $this->actingAs($this->admin)
            ->json('GET', '/api/admin/city?limit=3');

                 // assert
        $response->assertStatus(201);
        // dd($response);
        // $this->assertCount(1, json_decode($response->getContent()));
        $this->assertCount(3, $response->json()['data']);

    }

    public function test_store_cities()
        {
            // arrange
            $city = [
                'name'  => 'اسم',
            ];
    
            // act
            $response = $this->actingAs($this->admin)
                ->json('POST', '/api/admin/city/add', $city);
    
            // assert
            $response->assertStatus(201);
            
            $record = json_decode($response->getContent(), true);
    
            # assert record created
            $this->assertDatabaseHas('cities', [
                'name' => $city['name'],
            ]);
        
        } 

        public function test_update_cities()
            {
                // arrange
                $city = factory('App\Models\City')->create();
        
                $form = [
                    'name'  => 'اسم',
                ];
        
                // act
                $response = $this->actingAs($this->admin)
                    ->json('POST', "/api/admin/city/{$city->id}/update", $form);
        
                // assert
                $response->assertStatus(201);
        
                # assert record created
                $this->assertDatabaseHas('cities', [
                    'name'  => 'اسم',
                ]);
            }

            public function test_remove_cities()
                {
                    // arrange
                    $city = factory('App\Models\City')->create();
            
                    // act
                    $response = $this->actingAs($this->admin)
                        ->json('DELETE', "/api/admin/city/{$city->id}/delete");
            
                    // assert
                    $response->assertStatus(201);
            
                    $this->assertDatabaseMissing('cities', [
                        'id'   => $city->id,
                        'name' => $city->name,
                    ]);
                }
}


