<?php

namespace Tests\Feature;

use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Storage;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class AuthTest extends TestCase
{
    /**
     * A basic feature test example.
     *
     * @return void
     */

    use RefreshDatabase;

    private $admin;

    /**
     * create admin to perform our tests
     */
    public function setUp(): void
    {
        parent::setUp();

        $this->admin = \App\Models\Admin::create([
            'name'     => 'أدمن',
            'email'    => 'admin@admin.com',
            'password' => \Hash::make('secret')
        ]);

        $this->user = \App\Models\User::create([
            'name' => 'Hazem Gamal',
            'email' => 'hazemgamal55@yahoo.com',
            'password' => '123456789'
        ]);
    }

    public function test_signupWithEmail()
    {
        $auth = [
            'name' => 'hazem gamal',
            'email' => 'hazemgamal55@yahoo.com',
            'password' => '123456789'
        ];

        // act
        $response = $this->json('POST', '/api/auth/register-user-email/', $auth);
        $response->assertStatus(201);
        $record = json_decode($response->getContent(), true);
        $this->assertDatabaseHas('users', [
            'name' => $auth['name'],
            'email' => $auth['email'],
            'role' => 'user',
        ]);
    }

    public function test_Login()
    {
        $auth = [
            'email' => 'hazemgamal55@yahoo.com',
            'password' => '123456789'
        ];

        $response = $this->json('POST', '/api/auth/login/', $auth);
        $response->assertStatus(401);
        $record = json_decode($response->getContent(), true);
        $this->assertDatabaseHas('users', [
            'email' => $auth['email'],
        ]);
    }

    public function logout()
    {
        $this->user->email_verified_at = now();
        $this->user->save();

        $response = $this->actingAs($this->user)->json('POST', '/api/auth/logout/');
        $response->assertStatus(200);
    }

    public function test_forgetPassword()
    {
        $this->user->email_verified_at = now();
        $this->user->save();

        $auth = [
            'email' => 'hazemgamal55@yahoo.com'
        ];
        $response = $this->json('POST', 'api/auth/password/email', $auth);
        $response->assertStatus(201);
        $record = json_decode($response->getContent(), true);
        $this->assertDatabaseHas('users', [
            'email' => $auth['email'],
        ]);
    }

    public function test_updateUserPassword()
    {
        $this->user->email_verified_at = now();
        $this->user->save();

        $auth = [
            'password' => '111222111'
        ];
        $response = $this->actingAs($this->user)->json('POST', '/api/auth/update-password/', $auth);
        $response->assertStatus(201);
        $record = json_decode($response->getContent(), true);
        // $this->assertDatabaseHas('users', [
        //     'password' => \Hash::make($auth['password']),
        // ]);
    }

}
