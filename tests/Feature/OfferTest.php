<?php

namespace Tests\Feature;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Storage;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class OfferTest extends TestCase
{
    /**
     * A basic feature test example.
     *
     * @return void
     */

     use RefreshDatabase;

     private $admin;

     private $user;

     public function setUp(): void
    {
        parent::setUp();

        $this->admin = \App\Models\Admin::create([
            'name'     => 'أدمن',
            'email'    => 'admin@admin.com',
            'password' => \Hash::make('secret')
        ]);

        $this->user = \App\Models\User::create([
            'name'     => 'hazem gamal',
            'email'     => 'hazemgamal55@yahoo.com',
            'password'  => '123456789'
        ]);

        $category = factory('App\Models\Category', 3)->create();
        $city = factory('App\Models\City', 3)->create();
        $shop = factory('App\Models\Shop', 3)->create();
        $offer = factory('App\Models\Offer', 3)->create();

    }

    public function test_retrieve_offers()
    {
         factory('App\Models\Category', 3)->create();
         factory('App\Models\City', 3)->create();
         factory('App\Models\Shop', 3)->create();
         factory('App\Models\Offer', 3)->create();
        $response = $this->actingAs($this->user)
            ->json('GET', '/api/user/offers');

            $response->assertStatus(200);

            $this->assertCount(6, json_decode($response->getContent()));
    }


    public function test_update_offers()
    {

        $category = factory('App\Models\Category')->create();
        factory('App\Models\City')->create();
        $shop = factory('App\Models\Shop')->create();
        $offer = factory('App\Models\Offer')->create();

        $form = [
           'product_name'=> 'name',
           'image' => UploadedFile::fake()->image('0.jpg', 700, 700)->size(300),
           'price_before'=>12,
           'price_after'=>12,
           'shop_id'=>$shop->id,
           'category_id' => $category->id,
           'user_id' => $this->user->id,
       ];
       $response = $this->actingAs($this->admin)
       ->json('POST', "/api/shops/offers/{$offer->id}/update", $form);
       $response->assertStatus(201);

       $this->assertDatabaseHas('offers', [
        'product_name' => 'name',
        ]);

    }

    public function test_store_offers()
    {
        $category = factory('App\Models\Category')->create();
        factory('App\Models\City')->create();
       $shop = factory('App\Models\Shop')->create();

        $offer = [
            'product_name'=> 'product_name',
            'image' => UploadedFile::fake()->image('0.jpg', 700, 700)->size(300),
            'price_before'=>12,
            'price_after'=>12,
            'shop_id'=>$shop->id,
            'category_id' => $category->id,
            'user_id' => $this->user->id,
        ];

        $response = $this->actingAs($this->user)->json('POST', "/api/shops/offers/add", $offer);
        $response->assertStatus(201);
        $record = json_decode($response->getContent(), true);

        $this->assertDatabaseHas('offers', [
            'product_name'  => $offer['product_name'],
        ]);
    }

    public function test_show_offers()
    {
        $offer = factory('App\Models\Offer')->create();
        $response = $this->actingAs($this->admin)
        ->json('GET', "/api/user/offers/{$offer->id}");
        $response->assertStatus(200);
        $record = json_decode($response->getContent(), true);

    }

    public function test_remove_offer()
    {
        // arrange
        $offer = factory('App\Models\Offer')->create();

        // act
        $response = $this->actingAs($this->admin)
            ->json('DELETE', "/api/shops/offers/{$offer->id}/delete");

        // assert
        $response->assertStatus(201);

        $this->assertDatabaseMissing('offers', [
            'id'   => $offer->id,
            'product_name' => $offer->product_name,
        ]);
    }

}
