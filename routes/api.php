<?php

use Illuminate\Http\Request;

# ------------------------------------------------------------------------ #
#                               Authentication                             #
# ------------------------------------------------------------------------ #

Route::prefix('auth')->group(function () {
    Route::post('/register-user-email', 'AuthController@signupWithEmail');
    Route::post('/login', 'AuthController@login');
    Route::post('/password/email', 'AuthController@sendForgetPasswordEmail');
    Route::post('/reset/password', 'AuthController@reqConfirmCodeAndEmail');
    Route::post('/resend-confirm-code', 'AuthController@resendConfirmationCode');

    #------------------------------ Superadmin ----------------------------#
    Route::post('/superadmin/login', 'SuperadminAuthController@login');


    Route::middleware(['auth:sanctum'])->group(function () {

        Route::post('/update-email', 'AuthController@sendUpdateEmailAndConfrimCode');
        Route::post('/check-update-email', 'AuthController@checkConfirmCodeAndUpdateEmail');
        Route::post('/update-password', 'AuthController@updateUserPassword');
        Route::post('/logout', 'AuthController@logout');

        #------------------------------ Superadmin ----------------------------#
        Route::get('/superadmin/me/', 'SuperadminAuthController@me');
        Route::post('/superadmin/logout', 'SuperadminAuthController@logout');
    });
});




# ------------------------------------------------------------------------ #
#                              Superadmin routes                           #
# ------------------------------------------------------------------------ #
Route::group(['middleware' => ['auth:sanctum', 'admin'], 'prefix' => 'admin'], function () {
    #------------------------------- Users --------------------------------#
    Route::get('/users', 'Admin\UserController@index');
    Route::get('/users/count', 'Admin\UserController@count');
    Route::delete('/users/{id}/delete', 'Admin\UserController@destroy');
    Route::get('/users/search', 'Admin\UserController@search');
    Route::get('/users/ratio', 'Admin\UserController@ratio');

    #---------------------------- Category --------------------------------#
    Route::get('/category', 'Admin\CategoryController@index');
    Route::get('/category/find/all', 'Admin\CategoryController@indexAll');
    Route::post('/category/add', 'Admin\CategoryController@store');
    Route::post('/category/{category}/update', 'Admin\CategoryController@update');
    Route::delete('/category/{category}/delete', 'Admin\CategoryController@destroy');

    #-------------------------------- City -------------------------------#
    Route::get('/city/withShop', 'Admin\CityController@withShop');
    Route::get('/city', 'Admin\CityController@index');
    Route::post('/city/add', 'Admin\CityController@store');
    Route::post('/city/{id}/update', 'Admin\CityController@update');
    Route::delete('/city/{id}/delete', 'Admin\CityController@destroy');

    #-------------------------------- Offers -------------------------------#
    Route::get('/offers', 'Admin\OfferController@index');
    Route::get('/showOffer/{id}', 'Admin\OfferController@showOffer');
    Route::delete('/offers/{id}/delete', 'Admin\OfferController@delete');


    #-------------------------------- Shop -------------------------------#
    Route::get('/shops', 'Admin\ShopController@index');
    Route::get('/shops/find/all', 'Admin\ShopController@indexAll');
    Route::get('/shops/unverified', 'Admin\ShopController@unverified');
    Route::delete('/shops/{id}/delete', 'Admin\ShopController@destroy');
    Route::post('/shops/{id}/verify', 'Admin\ShopController@verify');
    Route::get('/shops/search', 'Admin\ShopController@search');

    #----------------------------- Settings ------------------------------#
    Route::post('/settings/{id}/update', 'Admin\SettingController@update');
    Route::get('/socialLinks', 'Admin\SettingController@retriveSocialMedia');
    Route::get('/pages/{key}', 'Admin\SettingController@getPage');
    Route::put('/pages/{key}', 'Admin\SettingController@updatePage');

    #----------------------------- Contact us ----------------------------#
    Route::get('/contact-us', 'Admin\ContactUsController@getMessages');
    Route::post('/contact-us/{id}/mark-as-seen', 'Admin\ContactUsController@markAsSeen');
    Route::delete('/contact-us/{id}', 'Admin\ContactUsController@remove');

    #----------------------------- Notifications ----------------------------#
    Route::get('/notification', 'Admin\NotificationController@index');
    Route::post('/send-notification', 'Admin\NotificationController@send');
    Route::delete('/notification/{id}/delete', 'Admin\NotificationController@delete');
});







# ------------------------------------------------------------------------ #
#                               Shop routes                                #
# ------------------------------------------------------------------------ #
Route::group(['middleware' => ['auth:sanctum', 'shop'], 'prefix' => 'shops'], function () {
    #-------------------------------- Shops -------------------------------#
    Route::post('/{id}/update', 'ShopController@update');
    Route::delete('/{id}/delete', 'ShopController@destroy');
    Route::get('/coupons', 'ShopController@showShopCoupon');

    #-------------------------------- Offers ------------------------------#
    Route::get('/offers', 'OfferController@myOffers');
    Route::post('/offers/add', 'OfferController@add');
    Route::post('/offers/{id}/update', 'OfferController@update');
    Route::delete('/offers/{id}/delete', 'OfferController@delete');



});







# ------------------------------------------------------------------------ #
#                             User routes                                  #
# ------------------------------------------------------------------------ #
Route::prefix('user')->group(function () {

    #---------------------------------- Guest -----------------------------#
    Route::get('/category', 'CategoryController@index');
    Route::get('/category/{category}', 'CategoryController@show');

    Route::get('/shops', 'ShopController@index');
    Route::get('/shops/{shop}', 'ShopController@show');

    Route::get('/offers', 'OfferController@index');
    Route::get('/offers/{id}', 'OfferController@show');

    Route::get('/socialLinks', 'SettingController@retriveSocialMedia');
    Route::get('/pages/{key}', 'SettingController@getPage');

    Route::post('/contact-us', 'ContactUsController@store');

    Route::get('/city', 'CityController@index');
    Route::get('distance/{latitude}/{longitude}', 'UserController@distance');

    #------------------------ Authenticated user --------------------------#
    Route::middleware(['auth:sanctum'])->group(function () {

        #----------------------------- Favourites -----------------------------#
        Route::post('/offers/{id}/favourite', 'FavouriteController@add');
        Route::get('/favourites', 'FavouriteController@show');

        #--------------------------- User profile --------------------------#
        Route::get('/auth/me/', 'UserController@show');
        Route::post('/profile/update', 'UserController@update');

        #----------------------------- Coupons -----------------------------#
        Route::post('/coupons/apply-coupon', 'CouponController@coupon');
        Route::get('my/coupons', 'CouponController@showCoupons');
        Route::post('/coupons/rate', 'CouponController@rate');

        #---------- Set location, lang, device id & get notifications ---------#
        Route::post('/set-location', 'UserController@setLocation');
        Route::post('/set-device', 'UserController@setDeviceId');
        Route::post('/set-lang', 'UserController@setLang');
        Route::get('/notification', 'NotificationController@index');


        #--------------------------- Create shop ------------------------#
        Route::post('shops/add', 'ShopController@store');


    });
});
