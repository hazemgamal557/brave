<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::view('/', 'index');
Route::view('forgot_password', 'auth.reset_password')->name('password.reset');

Route::get('mailable', function () {
    $user = App\Models\User::find(31);

    return new App\Mail\SendConfirmCode($user);
});
